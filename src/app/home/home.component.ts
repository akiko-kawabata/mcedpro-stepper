import { Component, Output, Input, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { StepModel} from "../common/model/step.model";

@Component ({
  selector: 'home-component',
  templateUrl: 'home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnDestroy{
  @Output() out: EventEmitter<any> = new EventEmitter();
  @Input() in: number;
  public scenarios = ['CE => MR', 'CE => CR', 'Panel Review' ];
  public label="Scenario";
  public evaluatorSteps: StepModel[];
  public reviewerSteps : StepModel[];
  public othersSteps : StepModel[];
  public anotherSteps : StepModel[];
  public evaluatorSteps0 : StepModel[];
  public reviewerSteps0 : StepModel[];
  public othersSteps0 : StepModel[];
  public evaluatorSteps1 : StepModel[];
  public reviewerSteps1 : StepModel[];
  public othersSteps1 : StepModel[];
  public evaluatorSteps2 : StepModel[];
  public reviewerSteps2 : StepModel[];
  public othersSteps2 : StepModel[];


  public evaluatorCurrentIndex = 0;
  public reviewerCurrentIndex = 0;
  public othersCurrentIndex = 0;
  public hasStaticTooltip: boolean = true;
  public showReviewer: boolean = false;
  public selectedScenarioIndex: number = 0;
  public hideProcess: boolean = false;
  public isMercerReview: boolean = false;
  public reviewer:  string;
  public others1: string;
  public others2: string;

  public peerReviewSteps = [
    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Draft',
      others: 'Draft',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Add roles',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Add evaluations',
      evaluator: 'Add Evaluation',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Evaluation',
      reviewer: 'Add Evaluation',
      others: 'Add Evaluation',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Select a reviewer',
      evaluator: 'Select Reviewer',
      reviewer: 'Add Evaluation',
      others: 'Add Evaluation',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Select Reviewer',
      reviewer: 'Select Reviewer',
      others: 'Select Reviewer',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Peer Review',
      reviewer: 'Add Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add Review',
      evaluator: 'Peer Review',
      reviewer: 'Add Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Review',
      reviewer: 'Add Review',
      others: 'Add Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Return to Evaluator',
      evaluator: 'Add Review',
      reviewer: 'Select Reviewer or Return to Evaluator',
      others: 'Add Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Select Reviewer or Return to Evaluator',
      reviewer: 'Select Reviewer or Return to Evaluator',
      others: 'Select Reviewer or Return to Evaluator',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Acceptance',
      others: 'Acceptance',
      staticTooltip: true,
      showReviewer: true
    },
    {
      title: 'Add Final Evaluation',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Acceptance',
      others: 'Acceptance',
      staticTooltip: true,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Add Final Evaluation',
      others: 'Add Final Evaluation',
      staticTooltip: true,
      showReviewer: true
    }
  ];

  public mercerPeerReviewSteps = [

    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Draft',
      others: 'Draft',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Add roles',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Add evaluations',
      evaluator: 'Add Evaluation',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Evaluation',
      reviewer: 'Add Evaluation',
      others: 'Add Evaluation',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Select a reviewer',
      evaluator: 'Select Reviewer',
      reviewer: 'Add Evaluation',
      others: 'Add Evaluation',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Select Reviewer',
      reviewer: 'Select Reviewer',
      others: 'Select Reviewer',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Peer Review',
      reviewer: 'Add Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add Review',
      evaluator: 'Peer Review',
      reviewer: 'Add Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Select Internal Reviewer',
      evaluator: 'Peer Review',
      reviewer: 'Select Internal Reviewer',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add Internal Review',
      evaluator: 'Peer Review',
      reviewer: 'Add Internal Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Return to Mercer Reviewer',
      evaluator: 'Peer Review',
      reviewer: 'Return to Mercer Reviewer',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Select Mercer Review',
      evaluator: 'Peer Review',
      reviewer: 'Select Mercer Review',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Return to Client',
      evaluator: 'Peer Review',
      reviewer: 'Return to Client',
      others: 'Peer Review',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Acceptance',
      others: 'Acceptance',
      staticTooltip: true,
      showReviewer: true
    },
    {
      title: 'Add Final Evaluation',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Acceptance',
      others: 'Acceptance',
      staticTooltip: true,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Final Evaluation',
      reviewer: 'Add Final Evaluation',
      others: 'Add Final Evaluation',
      staticTooltip: true,
      showReviewer: true
    }
  ];

  public panelReviewSteps = [
    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Draft',
      others: 'Draft',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Add roles',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Add Role',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Select reviewers',
      evaluator: 'Select Reviewers',
      reviewer: 'Add Role',
      others: 'Add Role',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Save the draft set',
      evaluator: 'Select Reviewers',
      reviewer: 'Select Reviewers',
      others: 'Select Reviewers',
      staticTooltip: true,
      showReviewer: false
    },
    {
      title: 'Submit the draft set',
      evaluator: 'First Panellist',
      reviewer: 'First Panellist',
      others: 'First Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add Review',
      evaluator: 'First Panellist',
      reviewer: 'First Panellist',
      others: 'First Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'First Panellist',
      reviewer: 'First Panellist',
      others: 'First Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Second Panellist',
      reviewer: 'Second Panellist',
      others: 'Second Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add review',
      evaluator: 'Second Panellist',
      reviewer: 'Second Panellist',
      others: 'Second Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Second Panellist',
      reviewer: 'Second Panellist',
      others: 'Second Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Third Panellist',
      reviewer: 'Third Panellist',
      others: 'Third Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Add Review',
      evaluator: 'Third Panellist',
      reviewer: 'Third Panellist',
      others: 'Third Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Save the draft set',
      evaluator: 'Third Panellist',
      reviewer: 'Third Panellist',
      others: 'Third Panellist',
      staticTooltip: false,
      showReviewer: true
    },
    {
      title: 'Submit the draft set',
      evaluator: 'Third Panellist',
      reviewer: 'Third Panellist',
      others: 'Third Panellist',
      staticTooltip: false,
      showReviewer: true
    }
  ];

  constructor(private el: ChangeDetectorRef){

  }
  ngOnInit() {

    this.createStepper();
    this.evaluatorSteps = this.evaluatorSteps0;
    this.reviewerSteps = this.reviewerSteps1;
    this.othersSteps = this.othersSteps0;
    this.anotherSteps = this.reviewerSteps1;
    this.reviewer = 'Reviewer';
    this.hideProcess = true;
    this.others1 = 'Other authorised users';
    this.others2 = '';
  }

  ngOnChanges() {
    this.changeProcessStepper();
    this.evaluatorCurrentIndex = 0;
    this.reviewerCurrentIndex = 0;
    this.othersCurrentIndex = 0
    this.others1 = 'Other authorised users';
    this.others2 = '';
  }

  ngOnDestroy() {

  }

  changeProcessStepper() {
    if (this.selectedScenarioIndex == 1) {

      this.evaluatorSteps = this.evaluatorSteps0;
      this.reviewerSteps = this.reviewerSteps0;
      this.othersSteps = this.othersSteps0;
      this.hideProcess = false;
    } else if (this.selectedScenarioIndex == 0) {

      this.evaluatorSteps = this.evaluatorSteps0;
      this.reviewerSteps = this.reviewerSteps1;
      this.othersSteps = this.othersSteps0;
      this.anotherSteps = this.reviewerSteps1;
      this.hideProcess = true;
    } else {
      this.evaluatorSteps = this.evaluatorSteps2;
      this.reviewerSteps = this.reviewerSteps2;
      this.othersSteps = this.othersSteps2;
      this.hideProcess = false;
    }

    console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');
    console.log(this.hideProcess);
    console.log(this.evaluatorSteps0);
  }

  createStepper() {
    // M --- Main, S --- Sub
    // A --- Active, I --- Incomplete, C --- Complete

    //---------- CE => CR -----------------------------
    this.evaluatorSteps0 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'A',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',   parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer or Return to Evaluator',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',   parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.reviewerSteps0= [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'I', parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer or Return to Evaluator',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.othersSteps0 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'I', parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer or Return to Evaluator',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.reviewerSteps1= [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'I', parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Internal Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Add Internal Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Mercer Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Mercer Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Client',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];


    this.evaluatorSteps1 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'I', parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Internal Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Add Internal Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Mercer Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Mercer Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Client',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.othersSteps1 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'I', parent: 1, display: true},
      {index: 0, type: 'S', title: 'Add Evaluation',  status: 'I',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewer',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'Peer Review',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Internal Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Add Internal Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Mercer Reviewer',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Select Mercer Review',  status: 'I',  parent: 2, display: true},
      {index: 0, type: 'S', title: 'Return to Client',  status: 'I',  parent: 2, display: true},
      {index: 3, type: 'M', title: 'Acceptance',  status: 'I',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Final Evaluation',  status: 'I',  parent: 3, display: true},
      {index: 4, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.evaluatorSteps2 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'A',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewers',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'First Panellist',  status: 'I',   parent: 0, display: true},
      {index: 3, type: 'M', title: 'Second Panellist',  status: 'I',   parent: 0, display: true},
      {index: 4, type: 'M', title: 'Third Panellist',  status: 'I',   parent: 0, display: true},
      {index: 5, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.reviewerSteps2 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'A',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewers',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'First Panellist',  status: 'I',   parent: 0, display: true},
      {index: 3, type: 'M', title: 'Second Panellist',  status: 'I',   parent: 0, display: true},
      {index: 4, type: 'M', title: 'Third Panellist',  status: 'I',   parent: 0, display: true},
      {index: 5, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];

    this.othersSteps2 = [
      {index: 1, type: 'M', title: 'Draft',  status: 'A',  parent: 0, display: true},
      {index: 0, type: 'S', title: 'Add Role',  status: 'A',  parent: 1, display: true},
      {index: 0, type: 'S', title: 'Select Reviewers',  status: 'I',   parent: 1, display: true},
      {index: 2, type: 'M', title: 'First Panellist',  status: 'I',   parent: 0, display: true},
      {index: 3, type: 'M', title: 'Second Panellist',  status: 'I',   parent: 0, display: true},
      {index: 4, type: 'M', title: 'Third Panellist',  status: 'I',   parent: 0, display: true},
      {index: 5, type: 'M', title: 'Finalise',  status: 'I',  parent: 0, display: true}
    ];
  }

  onClick(event) {
    console.log(event);
    let currentStep;
    let currentStep2;
    let currentStep3;

    if (this.selectedScenarioIndex === 0) {
      this.showReviewer = this.mercerPeerReviewSteps[event].showReviewer;

      if (this.showReviewer) {
        this.evaluatorSteps = this.evaluatorSteps1;
        this.othersSteps = this.othersSteps1;
        this.others1 = 'Other authorised client users';
        this.others2 = 'Mercer users';
      }else {
        this.evaluatorSteps = this.evaluatorSteps0;
        this.othersSteps = this.othersSteps0;
        this.others1 = 'Other authorised users';
        this.others2 = '';
      }

      currentStep = this.mercerPeerReviewSteps[event].evaluator;
      currentStep2 = this.mercerPeerReviewSteps[event].reviewer;
      currentStep3 = this.mercerPeerReviewSteps[event].others;

      if (currentStep === 'Peer Review') {
        this.isMercerReview = true;

      }

      this.hasStaticTooltip = this.mercerPeerReviewSteps[event].staticTooltip;

    } else if (this.selectedScenarioIndex === 1){
      this.showReviewer = this.peerReviewSteps[event].showReviewer;
      currentStep = this.peerReviewSteps[event].evaluator;
      currentStep2 = this.peerReviewSteps[event].reviewer;
      currentStep3 = this.peerReviewSteps[event].others;
      this.hasStaticTooltip = this.peerReviewSteps[event].staticTooltip;
    } else {
      this.showReviewer = this.panelReviewSteps[event].showReviewer;
      currentStep = this.panelReviewSteps[event].evaluator;
      currentStep2 = this.panelReviewSteps[event].reviewer;
      currentStep3 = this.panelReviewSteps[event].others;
      this.hasStaticTooltip = this.panelReviewSteps[event].staticTooltip;

      if (currentStep2 === 'First Panellist') {
        this.reviewer = 'First Panellist';
      }else if (currentStep2 === 'Second Panellist') {
        this.reviewer = 'Second Panellist';
      }else {
        this.reviewer = 'Third Panellist';
      }

    }

    this.evaluatorCurrentIndex = this.evaluatorSteps.map(e=>e.title).indexOf(currentStep);
    this.reviewerCurrentIndex = this.reviewerSteps.map(e=>e.title).indexOf(currentStep2);
    this.othersCurrentIndex = this.othersSteps.map(e=>e.title).indexOf(currentStep3);

    console.log('Evaluator: ' + this.evaluatorCurrentIndex);
    console.log('Reviewer: ' + this.reviewerCurrentIndex);
    console.log('Others: ' + this.othersCurrentIndex);
    console.log(this.evaluatorSteps[this.evaluatorCurrentIndex]);
  }

    onSelected(event){
    this.selectedScenarioIndex = event;

    if (this.selectedScenarioIndex === 0 || this.selectedScenarioIndex === 1 ) {
      this.reviewer = 'Reviewer';
    } else {
      this.reviewer = 'First Panellist';
    }
    this.out.emit(this.selectedScenarioIndex);
  }
}
