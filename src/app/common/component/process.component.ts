import { Component, Input, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewChecked } from '@angular/core';

@Component ({
  selector: 'process-component',
  templateUrl: 'process.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class ProcessComponent{
  @Input() steps: any[];
  @Output() activeStepIndex: EventEmitter<any> = new EventEmitter();

  constructor(private el: ChangeDetectorRef) {
    setTimeout( () => this.el.detectChanges(), 10);
  }

  ngOnChanges() {

    console.log('--------ntonchnges');

    console.log(this.steps);


  }



  OnDestroy() {
    console.log('-------process-----DESTROY');
  }
  onClick(stepper) {

    console.log(stepper.activeStepIndex);
    this.activeStepIndex.emit(stepper.activeStepIndex);

  }
}
