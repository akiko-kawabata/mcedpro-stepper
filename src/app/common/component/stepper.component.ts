import { Component, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { StepModel} from "../model/step.model";

@Component ({
  selector: 'stepper-component',
  templateUrl: 'stepper.component.html',
  styleUrls: ['stepper.component.scss']
})

export class StepperComponent {
  @Input() steps: StepModel[];
  @Input() currentIndex: number;
  @Input() hasStaticTooltip: boolean;
  @Input() hideProcess: boolean;
  @Input() isMercerReview: boolean;
  @Input() isActiveUser: boolean;
  public numOfProgress: number;
  public completedProgress: number;
  @ViewChild ('stepCircle') public stepCircle;

  constructor(private el: ChangeDetectorRef) {}

  ngOnDestroy() {
    console.log('---------stepper---------DESTROY');
  }
  ngOnChanges() {
    console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'+this.isActiveUser);
    this.numOfProgress = (((this.currentIndex)/this.steps.length)*100)*0.98;
    this.updateStatus(this.currentIndex, this.steps);
    this.checkCompleteSteps();
    console.log(this.isMercerReview);
    console.log(this.steps);
  }

  hideSubProcess(steps, index) {
    let i = 0;
    if (steps[index].type == 'M') {
      i = index + 1;
    } else {
      i = index;
    }
    while(steps[i].parent != 0) {
      steps[i].display = false;
      i++;
    }
  }

  showAllProcess(steps) {
    for (let i = 0; i < steps.length; i++) {
      steps[i].display = true;
    }
  }

  checkCompleteSteps() {
    let completedSteps = this.steps.filter(function(step) { return step.status === 'C'});
    let mumOfCompletedSteps = completedSteps.length;
    this.completedProgress = ((mumOfCompletedSteps/this.steps.length)*100);
  }

  // A --- Active
  // C --- Complete
  // I --- Incomplete
  updateStatus(currentIndex, steps) {
    let nodeNumList = this.createNumberOfNodesList(steps);
    let lastMainIndex = steps[currentIndex].parent;
    let status = '';
    let completeIndex = 0;
    let stepIndex = 0;
    let activeIndex = 0;
    let inCompleteIndex = 0;
    let nextIndex = 0;

    if (steps[currentIndex].parent === 0) {
      for (completeIndex = 0; completeIndex < currentIndex; completeIndex++) {
        steps[completeIndex].status = 'C';
      }

      steps[currentIndex].status = 'A';

      for (inCompleteIndex = currentIndex + 1; inCompleteIndex < steps.length; inCompleteIndex++) {
        steps[inCompleteIndex].status = 'I';
      }
    }else {
      for (let mainIndex = 1; mainIndex< lastMainIndex; mainIndex++) {
        for (stepIndex = nextIndex; stepIndex< nextIndex + nodeNumList[mainIndex-1]; stepIndex++) {
          steps[stepIndex].status = 'C';
        }
        nextIndex = stepIndex;
      }

      for (activeIndex = stepIndex; activeIndex <= currentIndex; activeIndex++) {
        steps[activeIndex].status = 'A';
      }

      for (inCompleteIndex = activeIndex; inCompleteIndex < steps.length; inCompleteIndex++) {
        steps[inCompleteIndex].status = 'I';
      }
    }

    console.log(this.hideProcess)
    console.log('currentIndex: ' + this.currentIndex +'/' + this.steps[this.currentIndex].title);
    if (this.hideProcess &&  (this.steps[this.currentIndex].title === 'Peer Review' ||
        this.steps[this.currentIndex].title === 'Add Review'  ||
        this.steps[this.currentIndex].title === 'Select Reviewer or Return to Evaluator' ||
        this.steps[this.currentIndex].title === 'Select Internal Reviewer' ||
        this.steps[this.currentIndex].title === 'Add Internal Review' ||
      this.steps[this.currentIndex].title === 'Return to Mercer Reviewer' ||
      this.steps[this.currentIndex].title === 'Select Mercer Review' ||
      this.steps[this.currentIndex].title === 'Return to Client')){

      // Hide Peer review processes for evaluator
      this.hideSubProcess(this.steps, this.currentIndex);

      this.updateProcessLine(this.steps, this.currentIndex);
    }

    if (!this.isActiveUser && (this.steps[this.currentIndex].title === 'First Panellist' ||
                               this.steps[this.currentIndex].title === 'Second Panellist' ||
                               this.steps[this.currentIndex].title === 'Third Panellist' )) {
      this.updateProcessLine(this.steps, this.currentIndex);
    }
  }

  updateProcessLine(steps, currentIndex) {
    let nextMainIndex = 0;

    if (steps[currentIndex].parent === 0) {
      nextMainIndex = steps[currentIndex].index + 1;
    } else {
      nextMainIndex = steps[currentIndex].parent + 1;
    }

    console.log(nextMainIndex);

    let index =  steps.map(e=>e.index).indexOf(nextMainIndex);
    this.numOfProgress = (((index)/this.steps.length)*100);
  }

  createNumberOfNodesList(steps) {
    let temp;
    let nodeNumList = [];
    let numOfParents = steps.filter(function (item) {return item.type === 'M'}).length;

    for (let i = 0; i < numOfParents; i++) {
      temp = steps.filter(function(item) {return item.parent === i+1}).length+1;
      nodeNumList.push(temp);
    }

    return nodeNumList;
  }

}
