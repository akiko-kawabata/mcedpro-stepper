import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component ({
  selector: 'comboBox-component',
  templateUrl: 'comboBox.component.html'
})

export class ComboBoxComponent {
  @Input() public items;
  @Input() public label;
  @Output() selectedOption = new EventEmitter();

  ngOnChanges() {

  }

  onChange(event) {
    console.log(event);
    console.log(event.target.selectedIndex);
    this.selectedOption.emit(event.target.selectedIndex);
  }
}
