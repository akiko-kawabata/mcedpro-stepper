import { Component, Input, ViewChild } from '@angular/core';
import { StepModel} from "../model/step.model";

@Component ({
  selector: 'step-component',
  templateUrl: 'step.component.html',
  styleUrls: ['step.component.css']
})

export class StepComponent {
  @Input() steps: StepModel[];
  @Input() currentIndex: number;
  @Input() hasStaticTooltip: boolean;
  @Input() isMercerReview: boolean;

  ngOnChanges() {
      console.log('isMercerReview' + this.isMercerReview);
  }



}
