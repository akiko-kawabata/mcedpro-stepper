import { ToolTipModel } from "./toolTip.model";

export class StepModel {
 public index: number;
 public type: string; // Main, Sub
 public title: string;
 public status: string;
 public parent: number;
 public display: boolean = true;
}



