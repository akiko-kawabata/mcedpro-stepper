export class ToolTipModel {
  public isStatic: boolean;
  public title: string;
}
