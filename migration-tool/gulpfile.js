const gulp = require('gulp');
const runSequence = require('run-sequence');

const tasks = [
  'pre-start',
  'delete-files',
  'modify-package',
  'copy-files',
  'configure-angular-cli',
  'refactor',
  'post-migrate'
];

for (const t of tasks) {
  require(`./tasks/${t}`);
}


// removes unnecessary files
gulp.task('default', () => {
  runSequence.apply(null, tasks);
});
