import * as path from 'path';

export const blueprintDirectory = path.join(__dirname, '../blueprint');

export function blueprintRelative(p) {
  return path.join(__dirname, '../blueprint', p);
}
