import * as gulp from 'gulp';
import { blueprintDirectory } from '../helpers/blueprint';
import { ROOT_PATH } from '../helpers/root';

const rootFiles = [
  '.editorconfig',
  '.gitignore',
  'e2e/**',
  'karma.conf.js',
  'protractor.conf.js',
  'tsconfig.json',
  'tslint.json',
];

const srcFiles = [
  'src/environments/**',
  'src/*.*'
];

gulp.task('copy-files', ['copy-files:root', 'copy-files:src']);

gulp.task('copy-files:root', () => {
  return gulp.src(rootFiles, { cwd: blueprintDirectory, base: 'blueprint' })
    .pipe(gulp.dest('.', { cwd: ROOT_PATH }));

});

gulp.task('copy-files:src', () => {
  return gulp.src(srcFiles, { cwd: blueprintDirectory, base: 'blueprint' })
    .pipe(gulp.dest('.', { cwd: ROOT_PATH }));
});
