import * as gulp from 'gulp';
import * as fs from 'fs';
import { join } from 'path';
import { rootRelative } from '../helpers/root';

import * as sortPackageJson from 'sort-package-json';

const packagesToRemove = new Set([
  '@angularclass/conventions-loader',
  '@angularclass/hmr',
  '@angularclass/hmr-loader',
  '@types/hammerjs',
  '@types/jasmine',
  '@types/jasminewd2',
  '@types/node',
  '@types/selenium-webdriver',
  '@types/source-map',
  '@types/uglify-js',
  '@types/webpack',
  'add-asset-html-webpack-plugin',
  'angular2-template-loader',
  'assets-webpack-plugin',
  'autoprefixer',
  'awesome-typescript-loader',
  'classlist.js',
  'codelyzer',
  'console-polyfill',
  'copy-webpack-plugin',
  'core-js',
  'cross-env',
  'css-loader',
  'cz-conventional-changelog',
  'exports-loader',
  'expose-loader',
  'extract-text-webpack-plugin',
  'file-loader',
  'find-root',
  'gh-pages',
  'html-loader',
  'html-webpack-plugin',
  'http-server',
  'ie-shim',
  'imports-loader',
  'inline-manifest-webpack-plugin',
  'istanbul-instrumenter-loader',
  'jasmine-core',
  'jasmine-spec-reporter',
  'json-loader',
  'karma',
  'karma-chrome-launcher',
  'karma-cli',
  'karma-coverage',
  'karma-coverage-istanbul-reporter',
  'karma-jasmine',
  'karma-jasmine-html-reporter',
  'karma-mocha-reporter',
  'karma-phantomjs-launcher',
  'karma-remap-coverage',
  'karma-sourcemap-loader',
  'karma-webpack',
  'merceros-ui-assets',
  'merceros-ui-components',
  'merceros-ui-core',
  'ng-router-loader',
  'ngc-webpack',
  'node-sass',
  'npm-run-all',
  'optimize-js-plugin',
  'parse5',
  'postcss-loader',
  'postcss-url',
  'protractor',
  'raw-loader',
  'reflect-metadata',
  'rimraf',
  'rxjs',
  'sass-loader',
  'script-ext-html-webpack-plugin',
  'source-map-loader',
  'standard-changelog',
  'string-replace-loader',
  'style-loader',
  'svgxuse',
  'to-string-loader',
  'ts-node',
  'tslib',
  'tslint',
  'tslint-loader',
  'typedoc',
  'typescript',
  'url-loader',
  'web-animations-js',
  'webpack',
  'webpack-bundle-analyzer',
  'webpack-dev-middleware',
  'webpack-dev-server',
  'webpack-dll-bundles-plugin',
  'webpack-merge',
  'zone.js',
]);

const scriptsToRemove = new Set([
  'build:aot:prod',
  'build:aot',
  'build:dev',
  'build:docker',
  'build:prod',
  'build',
  'changelog',
  'ci:aot',
  'ci:jit',
  'ci:nobuild',
  'ci:testall',
  'ci:testall:noe2e',
  'ci:travis',
  'ci',
  'clean:dll',
  'clean:aot',
  'clean:dist',
  'clean:install',
  'clean',
  'docker',
  'docs',
  'e2e:live',
  'e2e:travis',
  'e2e',
  'github-deploy:dev',
  'github-deploy:prod',
  'github-deploy',
  'lint',
  'lint:fix',
  'mos-migrate',
  'node',
  'postinstall',
  'preclean:install',
  'protractor',
  'protractor:dom',
  'protractor:delay',
  'protractor:live',
  'rimraf',
  'server:dev:hmr',
  'server:dev',
  'server:prod',
  'server:prod:ci',
  'server',
  'start:hmr',
  'start',
  'test',
  'test:dom',
  'tslint',
  'tslint:fix',
  'typedoc',
  'version',
  'postversion',
  'watch:dev:hmr',
  'watch:dev',
  'watch:prod',
  'watch:test',
  'watch',
  'webdriver-manager',
  'webdriver:start',
  'webdriver:update',
  'webpack-dev-server',
  'webpack',
]);

function deletePackages(dependencies) {
  for (const key of Object.keys(dependencies)) {
    if (packagesToRemove.has(key)) {
      delete dependencies[key];
    }
  }
}

function modifyScripts(scripts) {
  for (const key of Object.keys(scripts)) {
    if (scriptsToRemove.has(key)) {
      delete scripts[key];
    }
  }
  const newScripts = {
    'build': 'ng build',
    'ci': 'ng lint && ng test --single-run --code-coverage --browser=ChromeHeadless --no-colors && ng build --prod',
    'e2e': 'ng e2e',
    'lint': 'ng lint',
    'start': 'ng serve',
    'start:hmr': 'ng serve --hmr -e=hmr',
    'test': 'ng test'
  };

  for (const key of Object.keys(newScripts)) {
    scripts[key] = newScripts[key];
  }
}

gulp.task('modify-package', (cb) => {
  const packageJson = require('../../package.json');
  const dependencies = packageJson.dependencies;
  let devDependencies = packageJson.devDependencies;
  const scripts = packageJson.scripts;

  // NOTE: From https://docs.npmjs.com/files/package.json#license
  // Finally, if you do not wish to grant others the right to use a private or unpublished package under any terms:

  // { "license": "UNLICENSED"}
  // Consider also setting "private": true to prevent accidental publication
  packageJson.license = 'UNLICENSED';
  packageJson.private = true;

  // Configure dependencies and retain project specfic dependencies
  if (dependencies) {
    deletePackages(dependencies);
  }
  if (devDependencies) {
    deletePackages(devDependencies);
  } else {
    devDependencies = packageJson.devDependencies = {};
  }
  // add current @angular/cli version (this is required because @angular/cli will check the package.json for the version)
  devDependencies['@angular/cli'] = '1.6.5';
  // add karma in, due to some weird discrepancy of how npm handles installations, this needs to be here
  // to ensure that karma will install correctly at the top level
  devDependencies['karma'] = '~2.0.0';
  // add meta package, parent packages should retain the dependencies
  dependencies['ngpd-merceros-ui-meta'] = 'git+ssh://git@bitbucket.org/oliverwymantechssg/ngpd-merceros-ui-meta.git';

  if (scripts) {
    modifyScripts(scripts);
  }

  // clean up package.json of old angular-starter entries to match angular cli
  [
    'description',
    'keywords',
    'author',
    'homepage',
    'repository',
    'bugs',
    'engines'
  ].forEach((key) => {
    delete packageJson[key];
  });

  fs.writeFile(rootRelative('package.json'), JSON.stringify(sortPackageJson(packageJson), null, '  '), (err) => {
    if (err) {
      return cb(err);
    }
    cb();
  });
});
