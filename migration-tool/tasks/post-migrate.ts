import * as gulp from 'gulp';
import * as del from 'del';
import { join } from 'path';

const filesToRemove = [
  // remove angular starter config directory
  'config',

  // remove node_modules directory
  'node_modules',
].map(p => join('..', p));

gulp.task('post-migrate', async () => {
  const paths = await del(filesToRemove, { force: true });
  console.log('Deleted files and folders:\n', paths.join('\n'));
});
